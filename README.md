# React Todo List
This is a basic todo list created with react.

## Instructions

First clone this repository.
```bash
git clone https://gitlab.com/carlosthompson/todo-list-react.git
```

Install dependencies.
```bash
npm install
```

In the project directory, you can run:
```bash
npm start
```
